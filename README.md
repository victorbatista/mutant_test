**MUTANTBR NODEJS TEST**

## Requirements

1. docker version 18.09.6 - https://docs.docker.com/install/
2. docker-compose version 1.24.0 - https://docs.docker.com/compose/install/
3. Ports: 8080 - NodeJs, 9200 - ElasticSearch

---

## To start application

The steps to start the application.

1. To start application run `docker-compose up`.
2. Just access http:localhost:8080/.
3. It's using Nodemon, if you make some change in code, just refresh your webpage.