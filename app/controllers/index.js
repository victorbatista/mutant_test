const axios = require('axios');

function compareValues(key, order = 'asc') {
    return function (a, b) {
        if (!a.hasOwnProperty(key) ||
            !b.hasOwnProperty(key)) {
            return 0;
        }

        const aux1 = (typeof a[key] === 'string') ?
            a[key].toUpperCase() : a[key];
        const aux2 = (typeof b[key] === 'string') ?
            b[key].toUpperCase() : b[key];

        let comparison = 0;
        if (aux1 > aux2) {
            comparison = 1;
        } else if (aux1 < aux2) {
            comparison = -1;
        }
        return (
            (order == 'asc') ?
            comparison : (comparison * -1)
        );
    };
}

async function getUsersData() {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        return response.data;
    } catch (e) {
        console.log(e);
        return e.message;
    }
}

module.exports = {
    getUsers: async (req, resp) => {
        try {
            let usersRequest = await getUsersData();
            let users = [];
            for (user of usersRequest) {
                if (user.address.suite.toUpperCase().indexOf('SUITE') > -1) {
                    users.push({
                        name: user.name,
                        website: user.website,
                        email: user.email,
                        company: user.company.name,
                        address_suite: user.address.suite
                    });
                }
            }
            users = users.sort(compareValues('name', 'asc'));
            resp.status(200).send(users);
        } catch (e) {
            console.log(e);
            resp.status(500).send(e.message);
        }
    },
    getSuiteAddress: async (req, resp) => {
        let usersRequest = await getUsersData();
        let users = [];
        for (user of usersRequest) {
            console.log(user.website);
        }
        resp.status(200).send(users);
    }
}