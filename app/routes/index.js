const app = require('express').Router();
const {
    join
} = require('path');
const controller = require(join(__dirname, '../controllers/index'));

app.get('/users', async (req, res) =>
    await controller.getUsers(req, res));

app.get('/websites', async (req, res) =>
    await controller.getWebSites(req, res));

app.get('/suite', async (req, res) =>
    await controller.getSuites(req, res));

module.exports = app;